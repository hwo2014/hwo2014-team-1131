import math

# Race constants.
TIME_CONSTANT = 50.0
MAX_VELOCITY = 10.0
MAX_ACCELERATION = 0.2

# Race variables.
game_tick = -1
start_tick = -2
my_bot = 'HUEHUEHUE'
all_bots_info = {}
pieces = []
lanes = []
infos_name = {'color', 'weight', 'angle', 'piecePosition', 'distance', 'velocity', 'acceleration', 'nextSwitch'}

# Update game tick.
def set_game_tick(msg):
    if msg['msgType'] == 'carPositions':
        global game_tick
        game_tick = msg['gameTick'] if msg.get('gameTick') else -1

# Initialize variables.
def initialize_info(data, my_bot_name):
    global pieces
    pieces = data['race']['track']['pieces']
    global lanes
    lanes = data['race']['track']['lanes']
    global my_bot
    my_bot = my_bot_name
    
    for bot in data['race']['cars']:
        info = {}
        bot_name = bot['id']['name']
        
        for info_name in infos_name:
            info[info_name] = 0
        
        info['color'] = bot['id']['color']
        info['weight'] = bot['dimensions']['length'] * bot['dimensions']['width']
        info['nextSwitch'] = get_next_switch(0)
        
        all_bots_info[bot_name] = info

# Initialize correct car position.
def initialize_positions(data):
    for bot in data:
        bot_name = bot['id']['name']
        all_bots_info[bot_name]['piecePosition'] = bot['piecePosition']

# Read "carPositions" data and update stored information.
def read_car_positions_msg(data):
    global MAX_ACCELERATION
    global MAX_VELOCITY
    global TIME_CONSTANT
    for bot in data:
        info = {}
        bot_name = bot['id']['name']
        last_info = all_bots_info[bot_name]
        
        info['angle'] = angle = bot['angle']
        info['piecePosition'] = bot['piecePosition']
        
        info['velocity'] = get_velocity(info['piecePosition'], last_info)
        info['distance'] = last_info['distance'] + info['velocity']
        info['acceleration'] = info['velocity'] - last_info['velocity']
        
        if bot_name == my_bot:
            global start_tick
            # save tick that bot started moving
            if start_tick == -2 and info['velocity'] > 0:
                start_tick = game_tick
            
            # setting race constants after bot started moving
            if game_tick == start_tick:
                MAX_ACCELERATION = info['velocity']
                print 'Max acceleration is ' + str(MAX_ACCELERATION)
            elif game_tick == start_tick + 1:
                MAX_VELOCITY = (last_info['velocity']**2) / (2 * last_info['velocity'] - info['velocity'])
                TIME_CONSTANT = 1 / math.log(MAX_VELOCITY / (MAX_VELOCITY - last_info['velocity']))
                print 'Max velocity is ' + str(MAX_VELOCITY)
                print 'Time constant is ' + str(TIME_CONSTANT)
                start_tick = -3 # preventing constants from being set again after qualifying session
        
        all_bots_info[bot_name].update(info)

# Get piece length considering a given lane.
def get_piece_length(piece_index, lane = -1):
    if pieces[piece_index].get('length'):
        length = pieces[piece_index]['length']
    else:
        delta_radius = lanes[lane]['distanceFromCenter'] if lane != -1 else 0
        radius = pieces[piece_index]['radius'] - delta_radius * sign(pieces[piece_index]['angle'])
        angle_radians = math.pi * abs(pieces[piece_index]['angle']) / 180
        length = radius * angle_radians
    return length

# Get velocity (delta position) between two positions.
def get_velocity(position, last_info):
    distance = position['inPieceDistance']
    last_position = last_info['piecePosition']
    
    if last_position == 0:
        return distance
    
    distance -= last_position['inPieceDistance']
    start_index = last_position['pieceIndex']
    end_index = position['pieceIndex']
    
    if start_index != end_index:
        if pieces[start_index].get('switch'):
            distance = last_info['velocity'] + last_info['acceleration'] # use same velocity to prevent errors
        else:
            distance += get_piece_length(start_index, last_position['lane']['endLaneIndex'])
    return distance

# Get optimal throttle to reach a given velocity.
def go_to_velocity(bot_name, velocity):
    bot_info = all_bots_info[bot_name]
    delta_time = go_to_velocity_time(bot_info['velocity'], velocity)
    
    # if we have at least about 0.6 ticks until target velocity and
    # acceleration (therefore velocity) doesn't have an absurd value
    if delta_time >= 0.6:
        if velocity > bot_info['velocity']:
            throttle = 1
        else:
            throttle = 0
    else: # constant throttle value for target velocity
        throttle = velocity / MAX_VELOCITY
    return throttle

# Get time in ticks necessary to change velocity.
def go_to_velocity_time(velocity, target_velocity):
    if velocity < target_velocity: # acceleration
        if target_velocity > 0.9 * MAX_VELOCITY:
            target_velocity *= 0.98
        velocity_ratio = (velocity - MAX_VELOCITY) / (target_velocity - MAX_VELOCITY)
    else: # deacceleration
        if target_velocity < 0.1 * MAX_VELOCITY:
            target_velocity += 0.02
        velocity_ratio = velocity / target_velocity
    return TIME_CONSTANT * math.log(velocity_ratio) if velocity_ratio != 0 else 0

# Get distance traveled to change velocity.
def go_to_velocity_distance(velocity, target_velocity):
    time = go_to_velocity_time(velocity, target_velocity)
    time_exp = math.exp(time / TIME_CONSTANT) - 1
    if velocity < target_velocity: # acceleration
        distance = TIME_CONSTANT * time_exp * (target_velocity - MAX_VELOCITY) + time * MAX_VELOCITY
    else: # deacceleration
        distance = TIME_CONSTANT * time_exp * target_velocity
    return distance
    
# Get best throttle value for a bot.
def get_throttle(bot_name):
    bot_info = all_bots_info[bot_name]
    
    safe_velocity = 6.2 # we need to calculate this value properly based on next curve
    if safe_velocity > MAX_VELOCITY:
        safe_velocity = MAX_VELOCITY
    
    distance = go_to_velocity_distance(bot_info['velocity'], safe_velocity)
    straight_distance = get_distance_until_curve(bot_name)
    
    # if we have time to go back to safe velocity, use throttle 1
    if straight_distance > distance:
        throttle = 1
    else: # else, go to safe velocity
        throttle = go_to_velocity(bot_name, safe_velocity)
    return throttle

# Return if the bot should "throttle", switch "Right"/"Left" or use "turbo".
def get_action(bot_name):
    bot_info = all_bots_info[bot_name]
    piece_index = bot_info['piecePosition']['pieceIndex']
    
    if piece_index == bot_info['nextSwitch'] - 1:
        bot_lane = bot_info['piecePosition']['lane']['endLaneIndex']
        next_curve = get_best_next_curve(bot_info['nextSwitch'], bot_lane)
        next_switch = get_next_switch(bot_info['nextSwitch'])
        all_bots_info[bot_name].update({'nextSwitch': next_switch})
        
        result_lane = get_lane_after_switch(next_curve, bot_lane)
        
        if result_lane > bot_lane:
            return "Right"
        elif result_lane < bot_lane:
            return "Left"
    return "throttle"

# Get next curved piece after the given index.
def get_next_curve(piece_index):
    for index in pieces_range(piece_index + 1):
        if pieces[index].get('angle'):
            return index

# Get next switch piece after the given index.
def get_next_switch(piece_index):
    for index in pieces_range(piece_index + 1):
        if pieces[index].get('switch'):
            return index

# Get longest curve between two switches after the given index.
def get_best_next_curve(piece_index, bot_lane):
    curve_index = -1
    best_curve_index = -1
    curve_len = 0
    best_curve_len = 0
    for index in pieces_range(piece_index + 1):
        if pieces[index].get('switch'):
            if curve_index != -1 and curve_len > best_curve_len:
                best_curve_index = curve_index
            if best_curve_index != -1:
                break
        elif pieces[index].get('angle'):
            lane = get_lane_after_switch(index, bot_lane)
            if curve_index != -1:
                if sign(pieces[index]['angle']) == sign(pieces[curve_index]['angle']):
                    curve_len += get_piece_length(index, lane)
                else:
                    if curve_len > best_curve_len:
                        best_curve_index = curve_index
                        best_curve_len = curve_len
                    curve_index = index
                    curve_len = get_piece_length(index, lane)
            else:
                curve_index = index
                curve_len = get_piece_length(index, lane)
        else:
            if curve_len > best_curve_len:
                best_curve_index = curve_index
                best_curve_len = curve_len
            curve_index = -1
            curve_len = 0
    return best_curve_index

# Get the distance from current position until next curved piece.
def get_distance_until_curve(bot_name):
    bot_info = all_bots_info[bot_name]
    piece_index = bot_info['piecePosition']['pieceIndex']
    next_curve = get_next_curve(piece_index)
    
    if pieces[piece_index].get('angle') and pieces[next_curve].get('angle'):
        return 0
    
    distance = -bot_info['piecePosition']['inPieceDistance']
    for index in pieces_range(piece_index, next_curve):
        distance += get_piece_length(index)
    return distance

# Get resulted lane after a switch in favor of a given curve.
# Value is limited by 0 and the length of lanes[].
def get_lane_after_switch(piece_index, bot_lane):
    delta_lane = sign(pieces[piece_index]['angle'])
    
    if 0 <= bot_lane + delta_lane < len(lanes):
        return bot_lane + delta_lane
    else:
        return bot_lane

# Auxiliar function that returns a range array for piece indexes.
# Returned range is looped to 0 when exceeds len(pieces).
def pieces_range(start_index = -1, end_index = -1):
    if start_index == -1:
        return range(len(pieces))
    if end_index == -1:
        end_index = start_index
    final_range = end_index if end_index > start_index else end_index + len(pieces)
    return [index % len(pieces) for index in range(start_index, final_range)]

# Auxiliar function that returns the sign of a value (-1 or 1).
def sign(value):
    return int(math.copysign(1, value))
