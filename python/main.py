import json
import socket
import sys
import game

class HUEBot(object):
    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

    def msg(self, msg_type, data):
        msg = {"msgType": msg_type, "data": data}
        if game.game_tick != -1:
            msg['gameTick'] = game.game_tick
        self.send(json.dumps(msg))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    # Default function to create/join quick race.
    # Server chooses the race conditions.
    def join(self):
        return self.msg("join", {
                            "name": self.name,
                            "key": self.key })

    # Create race with given conditions.
    def create_race(self):
        return self.msg("createRace", {
                            "botId": {
                                "name": self.name,
                                "key": self.key },
                            "trackName": "keimola",
                            "password": "HUEEEEEUH",
                            "carCount": 1 })

    # Join specific race.
    def join_race(self):
        return self.msg("joinRace", {
                            "botId": {
                                "name": self.name + "2",
                                "key": self.key },
                            "trackName": "keimola",
                            "password": "HUEEEEEUH",
                            "carCount": 2 })

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def turbo(self):
        self.msg("turbo", "Using turbo!")

    def switch_lane(self, direction):
        self.msg("switchLane", direction)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        #self.create_race()
        #self.join_race()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")

    def on_game_init(self, data):
        game.initialize_info(data, self.name)

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        if game.game_tick == -1:
            game.initialize_positions(data)
            return
        
        game.read_car_positions_msg(data)
        action = game.get_action(self.name)
        
        if action == "throttle":
            self.throttle(game.get_throttle(self.name))
        elif action == "turbo":
            self.turbo()
        else:
            self.switch_lane(action)

    def on_turbo_available(self, data):
        print("Turbo available")

    def on_crash(self, data):
        print("Someone crashed")

    def on_game_end(self, data):
        print("Race ended")

    def on_error(self, data):
        print("Error: {0}".format(data))

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'turboAvailable': self.on_turbo_available,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            game.set_game_tick(msg)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = HUEBot(s, name, key)
        bot.run()
